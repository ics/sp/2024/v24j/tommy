num_topics_min_value = 1
num_topics_max_value = 999
amount_of_words_min_value = 1
amount_of_words_max_value = 999
min_df_min_value = 0.0
min_df_max_value = 0.8
max_features_min_value = 1
max_features_max_value = 1_000_000_000
alpha_min_value = 0.0
beta_min_value = 0.0

"""
This program has been developed by students from the bachelor Computer Science
at Utrecht University within the Software Project course.
© Copyright Utrecht University
(Department of Information and Computing Sciences)
"""
